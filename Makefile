all: test_hashtable logdb lib/liblogdb.a lib/liblogdb.so cliente

test_hashtable: src/test_hashtable.c
	gcc  -Iinclude/ src/test_hashtable.c -o bin/test_hashtable

logdb:  obj/log_cierro.o obj/log_abro.o obj/log_inserto.o  obj/log_creo.o obj/logdb.o
	gcc $^ -o ./bin/logdb 

lib/liblogdb.a: obj/conectar_db.o  obj/abrir_db.o obj/put_val.o obj/cerrar_db.o obj/crear_db.o
	ar rcs $@ $^

lib/liblogdb.so: src/conectar_db.c src/abrir_db.c src/put_val.c src/cerrar_db.c src/crear_db.c
	gcc -shared -fPIC -o $@ $^

cliente: obj/cliente.o   lib/liblogdb.a
	gcc -static -o bin/cliente obj/cliente.o -llogdb -L lib lib/liblogdb.a

obj/cliente.o: src/cliente.c
	gcc -Wall -c $< -o $@

obj/logdb.o: ./src/logdb.c
	gcc -Wall -c $< -o $@ 

obj/log_abro.o: ./src/log_abro.c
	gcc -Wall -c $< -o $@

obj/log_inserto.o: ./src/log_inserto.c
	gcc -Wall -c $< -o $@

obj/log_cierro.o: ./src/log_cierro.c
	gcc -Wall -c $< -o $@

obj/log_creo.o: ./src/log_creo.c
	gcc -Wall -c $< -o $@

obj/conectar_db.o: ./src/conectar_db.c
	gcc -Wall -c $< -o $@	

obj/abrir_db.o: ./src/abrir_db.c
	gcc -Wall -c $< -o $@

obj/crear_db.o: ./src/crear_db.c
	gcc -Wall -c $< -o $@

obj/put_val.o: ./src/put_val.c
	gcc -Wall -c $< -o $@

obj/cerrar_db.o: ./src/cerrar_db.c
	gcc -Wall -c $< -o $@

.PHONY: clean
clean:
	rm bin/* obj/* lib/*
