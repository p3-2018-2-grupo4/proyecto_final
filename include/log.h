#include <stdio.h>
#include <unistd.h>
#include<string.h>
#include <sys/types.h>          /* some systems still require this */
#include <sys/stat.h>
#include <fcntl.h>
#include "../include/uthash.h"

typedef struct hash{
    char* clave;
    char *valor;
    UT_hash_handle hh; //necesario para poder usar la estrutura en la hashtable.
}hash_db;

int log_abro(char * nombre);
int log_creo(char * nombre);
int log_inserto(int fd, char* clave, int valc,char* valor,int valv);
void log_cierro(int fd);	


