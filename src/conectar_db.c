//Uso de la cabecera que contiene la informacion
#include "../include/logdb.h"
#include <stdio.h>

#include <sys/types.h>          /* some systems still require this */
#include <sys/stat.h>
#include <stdio.h>              /* for convenience */
#include <stdlib.h>             /* for convenience */
#include <stddef.h>             /* for offsetof */
#include <string.h>             /* for convenience */
#include <unistd.h>             /* for convenience */
#include <signal.h>             /* for SIG_ERR */ 
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#define MAXSLEEP 64

int connect_retry( int domain, int type, int protocol, 	const struct sockaddr *addr, socklen_t alen){
	
	int numsec, fd; /* * Try to connect with exponential backoff. */ 

	for (numsec = 1; numsec <= MAXSLEEP; numsec <<= 1) { 

		if (( fd = socket( domain, type, protocol)) < 0) 
			return(-1); 

		if (connect( fd, addr, alen) == 0) { /* * Conexión aceptada. */ 
			return(fd); 
		} 
		close(fd); 				//Si falla conexion cerramos y creamos nuevo socket

		/* * Delay before trying again. */
		if (numsec <= MAXSLEEP/2)

			sleep( numsec); 
	} 
	return(-1); 
}

conexionlogdb * conectar_db(char *mip, int p ){
//Separamos espacio para la estructura conexion
conexionlogdb * conexion = (conexionlogdb *)malloc(sizeof(conexionlogdb ));
//Asignamos valores a 
conexion-> ip =  mip ;
conexion-> puerto =p ;
//Se declara el socket
int sockfd;
//Direccion del servidor
struct sockaddr_in direccion_cliente;
memset(&direccion_cliente, 0, sizeof(direccion_cliente));	//ponemos en 0 la estructura
//llenamos los campos
direccion_cliente.sin_family = AF_INET;		//IPv4
direccion_cliente.sin_port = htons(p); //Convertimos el numero de puerto al endianness de la red
direccion_cliente.sin_addr.s_addr = inet_addr(mip) ;	//Nos tratamos de conectar a esta direccion

if (( sockfd = connect_retry( direccion_cliente.sin_family, SOCK_STREAM, 0, (struct sockaddr *)&direccion_cliente, sizeof(direccion_cliente))) < 0) { 
		printf("falló conexión\n"); 
		exit(-1);
	} 
//Creamos un buffer para recibir el valor de la sesion
char buf[5]={0}	;
int numerorecibo= recv(sockfd,buf,5,0);
if (numerorecibo < 0) 	
		perror("EL id de sesion no pudo ser recibido"); 
int *nrecibido = (int *) buf;
int valr= ntohl(*nrecibido);
//Asignamos el valor de la sesion a la estructura
conexion-> id_sesion = valr ;
conexion -> sockfd = sockfd ;
printf("EL valor recibido fue %d\n",valr);

//Retornamos la estructura , cuyo espacio en memoria fue separado con malloc
return conexion ;
}

