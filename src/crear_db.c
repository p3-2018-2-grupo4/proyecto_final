#include "../include/logdb.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>


int crear_db(conexionlogdb *conexion, char *nombre_db){
//Buffer para recibir el valor entero
char buf[5]= {0} ;
//Indicarle al servidor que se desea crear la base de datos
sleep(3);
send(conexion -> sockfd,"crear_db",9,0);
//Se espera un tiempo antes de enviar el nombre de la base de datos que se desear crear
sleep(3);
send(conexion -> sockfd,nombre_db,30,0);
//Recibimos el numero
int inst = recv(conexion-> sockfd,buf,4,0);
if(inst < 0){
		perror("Error al recibir el valor\n");	
}
int *recibido_ptr = (int *)buf;	
//El numero recibido hay que convertirlo del orden de red al orden del host			
int recibido = ntohl(*recibido_ptr);		
if(recibido == 0){
	conexion->nombredb = nombre_db ;
	printf("La base de datos fue creada con exito\n");
	return 0 ;
}
return -1;

}
