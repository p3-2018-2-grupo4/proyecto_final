#include "../include/logdb.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

void cerrar_db(conexionlogdb *conexion){
//Primero hay que enviar la instruccion de querer cerrar la conexion
sleep(3);
send(conexion -> sockfd,"cerrar_db",9,0);
sleep(3);
//Enviamos el nombre de la base de datos que se desea cerrar
send(conexion -> sockfd,conexion->nombredb,20,0);
sleep(1);
//Cerramos el socket
close(conexion->sockfd);
//Liberamos el espacio en memoria que ocupaba la estructura de conexion
free(conexion);
}
