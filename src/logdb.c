#include<stdio.h>
#include<stdlib.h>
#include "../include/log.h"
#include <sys/types.h>          /* some systems still require this */
#include <sys/stat.h>
#include <stdio.h>              /* for convenience */
#include <stdlib.h>             /* for convenience */
#include <stddef.h>             /* for offsetof */
#include <string.h>             /* for convenience */
#include <unistd.h>             /* for convenience */
#include <signal.h>             /* for SIG_ERR */ 
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <time.h>

unsigned int id_sesion ;

//Funcion para inicializar el servidor
int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen){
	int fd;
	int err = 0;
	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;
	if(bind(fd, addr, alen) < 0)
		goto errout;
	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){
		if(listen(fd, qlen) < 0)
			goto errout;
	}
	return fd;
errout:
	err = errno;
	close(fd);
	errno = err;
	return (-1);
}

//Funcion auxiliar que devuelve el Descriptor de archivo dependiendo del nombre que se envia
int numerofd(char arrayfd[][30], char * dbname){
	for(int i=0 ; i <30; i++){
		if(strcmp(arrayfd[i],dbname)==0){
			return i;
		}
	}
	return -1 ;
}

int main(int argc, char * argv[]){

int sockfd;

if(argc!=4){
	printf("Cantidad de argumentos invalidos\n logdb <directorio> <ip> <puerto>\n");
}
else{


int puerto = atoi(argv[3]);

//INICIO BORRAR
printf("Valor de puerto: %d \n",puerto);
printf("Valor de ip: %s \n",argv[2]);
printf("Directorio:%s \n",argv[1]);

//FIN BORRAR

//Direccion del servidor
	struct sockaddr_in direccion_servidor;
	//Ponemos en 0 la estructura, para evitar tener valores basura
	memset(&direccion_servidor, 0, sizeof(direccion_servidor));
	//llenamos los campos
	direccion_servidor.sin_family = AF_INET;
	direccion_servidor.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	direccion_servidor.sin_addr.s_addr = inet_addr(argv[2]) ;	//Nos vinculamos a la interface localhost o podemos usar INADDR_ANY para ligarnos A TODAS las interfaces

//inicalizamos servidor (AF_INET + SOCK_STREAM = TCP)
	if( (sockfd = initserver(SOCK_STREAM, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor), 1000)) < 0){	//Hasta 1000 solicitudes en cola 
		printf("Error al inicializar el servidor\n");	
	}
	int clfd= 0;
	while(1){
		//Variable que se encarga de permitir conocer que descriptor de archivo es el de la bd
		int fd ;
		//Arreglo de char que almacenara el nombre de la base de datos segun el descriptor de archivo que posee
		char arrayfd[30][30]= {0};
		//Variable control de funciones
		int control;
		//EL nombre que recibe la funcion que agregara la funcion, debe incluir la ruta
		char ruta [30] = {0};
		//Variable que se encargara de controlar cuando quiero dejar de recibir instrucciones
		int salgo = 0; //1 ES TRUE Y 0 ES FALSE	
		//Se crean 2 buffer, en el caso de tener que trabajar con 2 variables
		char buf[1000]={0};
		char buf2[1000]={0};
		//Se acepta la conexion de un cliente	
		clfd = accept(sockfd, NULL, NULL);
		//Transformamos el numero a big endian
		//Se asigna un valor aleatorio del 1 al 100 al id_sesion
		srand(time(NULL));
		int numero = 1+ rand() %((100+1)-1);
		id_sesion= htonl(numero);
		send( clfd, &id_sesion, sizeof(unsigned int), 0); 
		sleep(3);
		//Haremos que se reciba la instruccion
		int inst=recv(clfd,buf,30,0);	
		if(inst < 0){
			perror("Error al recibir la instruccion\n");	
		}
		
		else{
			while(salgo == 0){
				//En el caso de que se desee abrir una base de datos
				if(strcmp(buf,"abrir_db")==0){	
					printf("Se abrira la base de datos\n");
					//Se recibe el nombre de la base de datos
					inst = recv(clfd,buf,30,0);
					if(inst < 0){
						perror("Error al recibir la instruccion\n");	
					}
					
					//Se concatena el nombre de la db recibido con la ruta
					printf("La ruta que ingresaste es%s\n",ruta);
					strcat(ruta,argv[1]);
					strcat(ruta,"/");
					strcat(ruta,buf);
					printf("La ruta total es%s\n",ruta);
					//Se utilizar una funcion auxiliar, que abrira la base de datos
					fd = log_abro(ruta);
					//Se borra lo que tenia el array
					memset(ruta,0,30);
					//INICIO BORRAR
					printf("Se guardara en el arreglo de fds el nombre: %s\n",buf);
					//FIN BORRAR
					//Validar el valor que se le entregara a la funcion que abre la base de datos
					if(fd>0){
						//Se agrega al array de los descriptores de archivos el nombre de las base de datos
						strcpy(arrayfd[fd],buf);
						control= 0;						
					}
					//INICIO BORRAR
					printf("El valor del fd es: %d\n",fd);
					//FIN BORRAR
					// Se le envia a la funcion de abrir_bd el valor de -1 o 0 dependiendo de si la db pudo abrirse
					unsigned int num  = htonl(control);	//convertir del orden del host al orden de red. Entero sin signo de 32 bits.
					sleep(1);
					send( clfd, &num, sizeof(unsigned int), 0); 
					//Se vuelve a esperar una instruccion
					memset(buf,0,30);
					inst = recv(clfd,buf,30,0);
					if(inst < 0){
						perror("Error al recibir la instruccion\n");	
					}	
					
				}
					//En el caso de que se desee ingresar un valor
					else if(strcmp(buf,"put_val")==0){
					//Se libera el buffer, para no tener contenido basura, ya que se lo utilizara una vez mas
					memset(buf,0,1000);
					//Se recibe el nombre de la base de datos a la que se le desea ingresar un valor
					inst = recv(clfd,buf,30,0);
					if(inst < 0){
						perror("Error al recibir el nombre de la base de datos\n");	
					}
					//Uso de la funcion que me permite conocer el fd
					fd= numerofd(arrayfd,buf);
					//INICIO BORRAR
					printf("El fichero que corresponde a dicha base de datos es: %d\n",fd);
					//FIN BORRAR
					//Se libera el buffer
					memset(buf,0,1000);	
					//Se recibe la clave
					ssize_t res= read(clfd,buf,1000);
					if(res < 0){
						perror("Error al recibir la instruccion\n");	
					}
					int n1= res;
					//Se guarda en un puntero la clave
					char * c = buf ;
					//Se recibe el valor
					res = read(clfd,buf2,1000);
					if(res < 0){
						perror("Error al recibir la instruccion\n");	
					}
					int n2= res;
					char * v = buf2;
					printf("EL valor de clave  es %s y de valor es %s\n",c,v);
					//Se utiliza la funcion auxiliar, que se encarga de ingresar los valores
					log_inserto(fd,c,n1,v,n2);
					//Se vuelve a esperar una instruccion
					memset(buf,0,30);
					inst = recv(clfd,buf,30,0);
					if(inst < 0){
						perror("Error al recibir la instruccion\n");	
					}
					
					

				}
				//En el caso que el cliente quiera cerrar una conexion
				else if(strcmp(buf,"cerrar_db")==0){
					//Recibo el nombre de la base de datos que se desea cerrar
					inst = recv(clfd,buf,30,0);
					if(inst < 0){
						perror("Error al recibir la instruccion\n");	
					}
					//Uso de la funcion que me permite conocer el fd
					fd= numerofd(arrayfd,buf);
					//INICIO BORRAR
					printf("EL nombre de la base de datos que cerrare es: %s\n", arrayfd[fd]);
					//FIN BORRAR
					//Uso de la funcion auxiliar que cierra la base de datos
					log_cierro(fd);
					//Se quita del array de fd, el nombre de la base de datos
					strcpy(arrayfd[fd],"");
					//INICIO BORRAR
					printf("EL nombre de la base de datos del descriptor que ya cerre es: %s\n", arrayfd[fd]);
					printf("La base de datos ha sido cerrada\n");
					//FIN BORRAR
					//Se vuelve a esperar una instruccion
					memset(buf,0,30);
					inst = recv(clfd,buf,30,0);
					if(inst < 0){
						perror("Error al recibir la instruccion\n");	
					}
					
				
				}
				else if(strcmp(buf,"crear_db")==0){
					//Recibo el nombre de la base de datos que se desea crear
					inst = recv(clfd,buf,30,0);
					
					if(inst < 0){
						perror("Error al recibir el nombre de la bd\n");	
					}
					//Concanteno la ruta para que la db, pueda ser creada en ella
					strcat(ruta,argv[1]);
					strcat(ruta,"/");
					strcat(ruta,buf);
					//INICIO BORRAR
					printf("La ruta que se usara es %s\n",ruta);
					//FIN BORRAR
					fd= log_creo(ruta);
					//Se vacia el array de la ruta
					memset(ruta,0,30);
					//INICIO BORRAR					
					printf("El descriptor de archivo para la bd creada es: %d\n",fd);	
					//FIN BORRAR
					if(fd >= 0){	
						//Se agrega al array de los descriptores de archivos el nombre de las base de datos
						strcpy(arrayfd[fd],buf);
						control = 0;				
					}
					// Se le envia a la funcion de crear_bd el valor de -1 o 0 dependiendo de si la db pudo crearse
					int num  = htonl(control);	
					sleep(1);
					send( clfd, &num, sizeof(unsigned int), 0); 
					//Se vuelve a esperar una instruccion
					memset(buf,0,30);
					inst = recv(clfd,buf,30,0);
					if(inst < 0){
						perror("Error al recibir la instruccion\n");	
					}
					
					
				}


				else{
					printf("Esa instruccion no existe\n");
					printf("Esperare por otra: \n");
					break ;
					//Se vuelve a esperar una instruccion					
					memset(buf,0,30);
					inst = recv(clfd,buf,30,0);	
				}
			}			
		}
		
		
		}


	
		
	close(clfd);	
	}


	
	return 1;
}
