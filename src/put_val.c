#include "../include/logdb.h"
#include <stdio.h>
#include <sys/socket.h> 

int recorro(char * algo){
	int val = 0;
	for(int i= 0; i<30; i++){
		if(algo[i] == '\0'){
			return val;
		}
		else{
			val= val+1 ;
		}
	}
return val;
}

int put_val(conexionlogdb *conexion, char *clave, char *valor){
//Variable que nos permitira conocer el size de la variable
int size;  

//Primero hay que enviar la instruccion de querer insertar en la bd
sleep(3);
send(conexion -> sockfd,"put_val",8,0);
//Ahora se envia el nombre de la base de datos a la que deseamos agregar determinado valor
size= recorro(conexion->nombredb);
sleep(3);
send(conexion -> sockfd,conexion->nombredb,size,0);
//Luego se envian los valores de la clave y el valor
size = recorro(clave);
sleep(3);
send(conexion -> sockfd, clave ,size,0);
size = recorro(valor);
sleep(3);
send(conexion -> sockfd, valor,size,0);
//Si se agrego adecuadamente,se retorna 0

return 0 ;
}
