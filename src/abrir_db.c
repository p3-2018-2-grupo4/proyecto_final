#include "../include/logdb.h"
#include <stdio.h>
#include <sys/socket.h> 
#include <fcntl.h>

#include <stddef.h>             /* for offsetof */
#include <string.h>             /* for convenience */
#include <unistd.h>             /* for convenience */
#include <signal.h>             /* for SIG_ERR */ 
#include <netdb.h> 
#include <errno.h> 

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
int abrir_db(conexionlogdb *conexion, char *nombre_db){
//Buffer para recibir el valor entero
char buf[10]= {0} ;
//Primero hay que enviar la instruccion de querer abrir la bd
send(conexion -> sockfd,"abrir_db",9,0);
//Se espera un tiempo antes de enviar el nombre de la base de datos que se desea abrir
sleep(3);
send(conexion -> sockfd,nombre_db,30,0);
//Esperamos a recibir el valor que nos confirme si la base de datos se pudo abrir o no
sleep(1);
int inst = recv(conexion-> sockfd,buf,4,0);
if(inst < 0){
		perror("Error al recibir el valor\n");	
}
//Recibimos el numero
unsigned int *recibido_ptr = (unsigned int *)buf;	
//El numero recibido hay que convertirlo del orden de red al orden del host			
unsigned int recibido = ntohl(*recibido_ptr);		
if(recibido == 0){
	conexion->nombredb = nombre_db ;
	printf("La base de datos fue abierta con exito\n");
	return 0 ;
}		

return -1 ;
}
